package gahleitner.LS1;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class Demo {

    public static void main(String[] args) {
        printWebsite("https://test.erei.at/");
    }

    public static void printWebsite(String urlString) {
        try {
            URL url = new URL(urlString);
            Scanner s = new Scanner(url.openStream());

            while (s.hasNextLine()) {
                System.out.println(s.nextLine());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

