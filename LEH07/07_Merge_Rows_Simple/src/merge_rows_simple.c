
#include<stdlib.h>
#include<stdio.h>

#include"helper.h"

int main(int argc, char *argv[]) {
    if(argc != 4) { //Input error detection
        printf("Error occurred. Too few or many arguments given. "
        		"Use ./<executable> <output file name> <input file name> <input file name>");
        return EXIT_FAILURE;
    }
    char *inputFileNameFirst = argv[2]; //Gets first input filename from console
    char *inputFileNameSecond = argv[3]; //Gets second input filename from console
    char *outputFileName = argv[1]; //Gets output filename from console

    FILE *inputFileFirst = fopen(inputFileNameFirst, "r"); //Opens file by given filename
    FILE *inputFileSecond = fopen(inputFileNameSecond, "r");
    FILE *outputFile = fopen(outputFileName, "w+");

    //Checks if no error occurred during opening files
    if(inputFileFirst == NULL || inputFileSecond == NULL || outputFile == NULL) {
        printf("Error occurred while opening files.");
        return EXIT_FAILURE;
    }

    char line[MAX_LINE_LENGTH]; //temp variable
    do {
        fgets(line, MAX_LINE_LENGTH, inputFileFirst); //reads line from first input file
        if(fprintf(outputFile, "%s", line) <= 0) { //writes to output file, checks if errors occurred
        	printf("Error occurred while writing to file or reading from first input file.");
        }

        fgets(line, MAX_LINE_LENGTH, inputFileSecond); //reads line from second input file
        if(fprintf(outputFile, "%s", line) <= 0) { //writes to output file, checks if errors occurred
            printf("Error occurred while writing to file or reading from first input file.");
        }



    }   while(!feof(inputFileFirst) && !feof(inputFileSecond)); //checks if end of file reached, then exits loop


}
