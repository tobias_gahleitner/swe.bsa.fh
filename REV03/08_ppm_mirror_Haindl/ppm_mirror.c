/*
 ============================================================================
 Name        : ppm_mirror.c
 Author      : Michael Haindl
 Version     : 1
 Copyright   : none
 Description : Portable Pixel Map application supporting mirroring files.
 ============================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define MIN_ARG 5
#define MAX_HEIGHT 200
#define MAX_WIDTH 200

int main(int argc, char *argv[]) {

	// Check if minimal number of arguments is provided
	// otherwise inform the user and exit the program
	if (argc < MIN_ARG) {
		fprintf(stderr, "Error. Too few arguments.\n");
		fprintf(stderr, "Execution Example: $ ./ppm_mirror -o <output file name> -i <input file name> [-m <axis>]\n");
		fprintf(stderr, "-m; specifies mirroring axis, either h for horizontal or v for vertical\n");
		return EXIT_FAILURE;
	}

	//Pixel container for red, green and blue color values
	struct tPixel {
		int		r; /**< Red pixel value */
		int 	g; /**< Green pixel value */
		int		b; /**< Blue pixel value */
	};

	//PPM conform data structure
	struct tPPM {
		char* 			filename; 	/**< Pointer to PPM file name */
		char 			version[3];	/**< PPM Version */
		int 			height; 	/**< Image height */
		int 			width; 		/**< Image width */
		int 			max_val; 	/**< Image max. color value */
		struct tPixel 	pixel[MAX_WIDTH][MAX_HEIGHT];
									/**< Image pixel (r,g,b) data */
	};
	struct tPPM ppm;

	int opt; /**< initializer for parsing input parameter */
	char *outputfilename;

	//File-pointer
	FILE *inputfile = NULL;
	FILE *outputfile = NULL;
	int mode; /**< specifier for mirroring axis */
	mode = 0;

	//parse entered arguments
			while ((opt = getopt(argc, argv, "o:i:m:")) != -1) { //':' tells that those options will require an argument

				switch(opt) {

				//input file
				case 'i':
					inputfile = fopen(optarg, "r");
					ppm.filename = optarg;
					break;

				//output file
				case 'o':
					outputfile = fopen(optarg, "w+");
					outputfilename = optarg;
					break;

				//mirroring axis mode
				case 'm':
					if(strcmp(optarg, "v")==0) {
						mode = 1;
					} else if(strcmp(optarg, "h")==0) {
						mode = 2;
					} else {
						fprintf(stderr, "Warning, image will be copied.\n");
						fprintf(stderr, "Use either h for horizontal or v for vertical mirroring.\n");
					}
					break;

				default:
					fprintf(stderr,"Execution Example: $ ./ppm_mirror -o <output file name> -i <input file name> [-m <axis>]\n");
					return EXIT_FAILURE;
					break;
				}
			}

	//check if opened successfully
	if (inputfile == NULL || outputfile == NULL) {
		fprintf(stderr, "Error in file opening/writing.");
		return EXIT_FAILURE;
	}

	// Load ppm image data
	fscanf(inputfile, "%s %i %i %i", ppm.version, &ppm.width, &ppm.height, &ppm.max_val);

	int i;
	int j;
	for(j = 0; j < ppm.height; j++) {
		for(i = 0; i < (ppm.width); i++) {
			fscanf(inputfile, "%i %i %i ", &ppm.pixel[i][j].r, &ppm.pixel[i][j].g, &ppm.pixel[i][j].b);
		}
	}

	//create (new) ppm image
	fprintf(outputfile, "%s\n%i %i\n%i\n", ppm.version, ppm.width, ppm.height, ppm.max_val);

	if(mode == 0) { //just copy
		for(j = 0; j < ppm.height; j++) {
			for(i = 0; i < ppm.width; i++) {
				fprintf(outputfile, "%i %i %i ", ppm.pixel[i][j].r, ppm.pixel[i][j].g, ppm.pixel[i][j].b);
			}
			fprintf(outputfile, "\n");
		}
		printf("%s successfully copied to %s.\n", ppm.filename, outputfilename);
	}

	else if(mode == 1) { //mirror ppm image vertically
		for(j = 0; j < ppm.height; j++) {
			for(i = (ppm.width-1); i >= 0; i--) {
				fprintf(outputfile, "%i %i %i ", ppm.pixel[i][j].r, ppm.pixel[i][j].g, ppm.pixel[i][j].b);
			}
			fprintf(outputfile, "\n");
		}
		printf("%s successfully mirrored vertically to %s.\n", ppm.filename, outputfilename);
	} //TODO image gets negative

	else if(mode == 2) { //mirror ppm image horizontally
		for(j = (ppm.height-1); j >= 0; j--) {
			for(i = 0; i < ppm.width; i++) {
				fprintf(outputfile, "%i %i %i ", ppm.pixel[i][j].r, ppm.pixel[i][j].g, ppm.pixel[i][j].b);
			}
			fprintf(outputfile, "\n");
		}
		printf("%s successfully mirrored horizontally to %s.\n", ppm.filename, outputfilename);
	}

	return EXIT_SUCCESS;
}
