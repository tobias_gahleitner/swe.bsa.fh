//#include "functions.c"
#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

//Used funcion prototypes
extern void ConvertToDec(char *zahlString, int base);
extern void ConvertToBin(int zahlDec);
extern void ConvertToOct(int zahlDec);
extern void ConvertToHex(int zahlDec);

#endif
