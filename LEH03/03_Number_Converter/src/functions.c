/*
 * funcions.c
 *
 *  Created on: Sep 26, 2017
 *      Author: es
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


//Converts input to decimal number
void ConvertToDec(char *zahlString, int base){
	char *ptrConvert = "";
		int zahlDec = strtol(zahlString, &ptrConvert, base); //string to decimal

		//Error detection
		if (*ptrConvert != '\0') {
				printf("Decimal could not be converted.");

			}
		else	{
			printf("Dec: %8i", zahlDec); //Output number in Decimal format
			ConvertToBin(zahlDec); //Call function to convert Decimal number to Bin
			ConvertToOct(zahlDec); //Call function to convert Decimal to Octal
			ConvertToHex(zahlDec); //Call function to convert Decimal to Hex
		}
}

void ConvertToBin(int zahlDec) {
	int binLength = (int)log2((double)zahlDec) + 1; //Get length of bin number (and array)
	int binArray[binLength];

	int i;
	//Converts Decimal to bin number and saves it in array.
	for(i = 0; i < binLength; i++) {
			if(zahlDec % 2 == 0) {
				binArray[i] = 0;
				zahlDec = zahlDec / 2;
			}
			else {
				binArray[i] = 1;
				zahlDec = (zahlDec - 1) / 2;
			}
		}

	printf(" Bin: 0b");

	//Outputs bin number
	for(i = binLength - 1; i >= 0; i--) {
		printf("%i", binArray[i]);
	}
}

void ConvertToOct(int zahlDec) {
	int octLength = (int)(log((double)zahlDec)/log(8)) + 1; //Get length of octal number (and array)
	int octArray[octLength];

	int i;

	//Convert decimal to octal
	for(i = 0; i < octLength; i ++) {
		octArray[i] = zahlDec % 8; //modulo operator % returns rest of division
		zahlDec = (zahlDec - (zahlDec % 8)) / 8;
	}

	printf(" Oct: 0");

	//Outputs oct number
	for(i = octLength - 1; i >= 0; i--) {
		printf("%i", octArray[i]);
	}
}

void ConvertToHex(int zahlDec) {
	int hexLength = (int)(log((double)zahlDec)/log(16)) + 1; //Get length of hex number (and array)
		char hexArray[hexLength];

		int i;

		//Generates hex number from decimal number
		for(i = 0; i < hexLength; i ++) {
			int rest = zahlDec % 16;
			if(rest > 9) {

				switch (rest) {
				case 10:
					hexArray[i] = 'A'; //A eq 10 in decimal
					break;
				case 11:
					hexArray[i] = 'B'; //B eq 11 in decimal
					break;
				case 12:
					hexArray[i] = 'C';
					break;
				case 13:
					hexArray[i] = 'D';
					break;
				case 14:
					hexArray[i] = 'E';
					break;
				case 15:
					hexArray[i] = 'F';
					break;
				default:
					printf("Error at Conversion to Hex."); //Error detections
					return;
				}
			}
			else {
				hexArray[i] = (char)((zahlDec % 16) + 48);
			}
			zahlDec = (zahlDec - (zahlDec % 16)) / 16;
		}

		printf(" Hex: 0x");

		//Outputs hex number
		for(i = hexLength - 1; i >= 0; i--) {
			printf("%c", hexArray[i]);
		}
		printf("\n");
}

