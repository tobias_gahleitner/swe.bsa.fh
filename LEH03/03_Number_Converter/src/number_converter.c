/*
 ============================================================================
 Name        : number_converter.c
 Author      : <Your Name>
 Version     : <Version>
 Copyright   : <Your copyright notice>
 Description : Number converter in C.
 ============================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "functions.h"
//Include user defined functions
//#include "functions.h";
//#include "functions.c";


int main(int argc, char *argv[]) {

	//Check argument count
	if(argc != 3) {
		printf("Ungültige Anzahl an Argumenten.\nUse <executable> <format> <number>");
		return EXIT_FAILURE;
	}


	if(strcmp("dec", argv[1]) == 0) {
		ConvertToDec(argv[2], 10);
	}
	else if(strcmp("bin", argv[1]) == 0) {
		ConvertToDec(argv[2], 2);
	}
	else if(strcmp("oct", argv[1]) == 0) {
			ConvertToDec(argv[2], 8);
		}
	else if(strcmp("hex", argv[1]) == 0) {
			ConvertToDec(argv[2], 16);
		}
	else {
		printf("Ungültiges Format eingegeben.\nUse <executable> <format> <number>.");
		return EXIT_FAILURE;
	}

    return EXIT_SUCCESS;
}
