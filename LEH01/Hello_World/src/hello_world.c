/*
 ============================================================================
 Name        : Hello_World_Solution.c
 Author      : Tobias Gahleitner
 Version     : 1.0
 Copyright   : ©Tobias Gahleitner 2017
 Description : Writes Text to Console
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main(void) {
	printf("Hello Tobias Gahleitner.\nYour birthday is the 09.02.1997.\n");
	return EXIT_SUCCESS;
}
