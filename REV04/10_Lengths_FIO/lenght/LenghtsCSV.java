/*
 ============================================================================
 Name        : LengthsCsv.java
 Author      : PLASS.MAximilian
 Version     : 1.0
 Copyright   : <Your copyright notice>
 Description : Reads length records (from *.csv) and writes them to new file 
               with converted value (in m) and calculated difference to 
               reference length.
 ============================================================================
 */
package lenght;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;


public class LenghtsCSV {
public static final String DELIMITER = ";";
	
	public static void main(String[] args) {
		
		ArrayList<Lenght> lengths = new ArrayList<Lenght>();
			//read Filenames from Arguments
			final String INPUTFILENAME = args[0];
			final String OUTPUTFILENAME = args[1];
			//Create reference length.
			final Lenght REF = new Lenght(Double.parseDouble(args[2]),args[3]);
			
			//Terminal Output of read arguments
			System.out.println("Inputfile: "+INPUTFILENAME+" Outputfile: "+OUTPUTFILENAME+
					" Referenzlänge: "+REF.getValue()+" "+REF.getUnit()+"\n\n");
			
			// open file.
			try {
				Scanner scanner = new Scanner(new File(INPUTFILENAME));
				PrintWriter writer = new PrintWriter(new File(OUTPUTFILENAME));
				
				// read data from file.
				readFromFile(scanner,lengths);
				scanner.close();
				//Terminal Output of scanned Data
				for(int i = 0; i < lengths.size(); i++){
					lengths.get(i).printInformation();
				}			
				// write to new file.
				writeToFile(writer,lengths,REF);
				writer.close();
				
				// Catch exceptions.
			}catch (FileNotFoundException e){
				e.printStackTrace();
			}
	}
	
	/**
	 * @brief Reads from `stream` and adds entries to `list`.
	 * 
	 * @param stream Scanner providing read access to file.
	 * @param list   ArrayList to store Length entries.
	 */
	public static void readFromFile(Scanner stream, ArrayList<Lenght> list) {
		
		// Loop until last line.
		while (stream.hasNextLine()) {
			
			// Read line.
			String line = stream.nextLine();
			
			// Split data at `delimiter`.
			String[] data = line.split(DELIMITER);
			
			// Create `Length` and set data from line,Add `Length` to `list`
			list.add(new Lenght(Double.parseDouble(data[0]),data[1]));
		}
	}
	
	/**
	 * @brief Writes entries of `list` to `stream`.
	 * private
	 * @param stream PrintWriter providing write access to file.
	 * @param list	 ArrayList of Length entries.
	 */
	public static void writeToFile(PrintWriter stream, ArrayList<Lenght> list, Lenght other) {
		
		// For each Length in `list`, ...
			for(int i = 0; i< list.size(); i++){
			// Write data to Output File
				stream.format("%.2f; %s; %.2f m; %.3f m\n",
						list.get(i).getValue(),
						list.get(i).getUnit(),
						list.get(i).convertToMeter(),
						list.get(i).differenceTo(other));
			}	
	}
}
