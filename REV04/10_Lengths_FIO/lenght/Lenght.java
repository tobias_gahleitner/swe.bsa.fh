/*
 ============================================================================
 Name        : Lengths.java
 Author      : PLASS Maximilian
 Version     : 1.0
 Copyright   : <Your copyright notice>
 Description : Length` storing measurement results, and providing conversion operations.
 ============================================================================
 */
package lenght;

public class Lenght {
	private String mUnit;
	private double mValue;
	
	public static final String INCH = "in";
	public static final String KM = "km";
	public static final String M = "m";
	
	public String getUnit(){
		return mUnit;
	}
	public double getValue(){
		return mValue;
	}
	public void setUnit(String Unit){
		this.mUnit = Unit;
	}
	public void setValue(double Value){
		this.mValue = Value;
	}
	public double convertToMeter(){
		if(getUnit().equals(INCH)){
			return getValue() * 0.0254;
		}
		else if( getUnit().equals(KM)){
			return getValue() * 1000;
		}
		else if(getUnit().equals(M)){
			return getValue();
		}
		else{
			return -1;	
		}
	}
	public Lenght(){
	}
	public Lenght(double lenght, String unit){
		setValue(lenght);
		setUnit(unit);
	}
	public double differenceTo(Lenght lenght){
		double l1 = this.convertToMeter();
		double l2 = lenght.convertToMeter();
		return l1 - l2;
	}
	public void printInformation(){
		System.out.println(getValue()+" "+getUnit());
	}
}
