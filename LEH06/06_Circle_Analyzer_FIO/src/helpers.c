/*
 * helpers.c
 *
 *  Created on: Oct 18, 2017
 *      Author: es
 */

#include<math.h>
#include<stdio.h>
#include <stdlib.h>
#include"helpers.h"

int GetCircleRadius_m(struct Circle *circleArray,int circleCount) {
	int i;
	for(i = 0; i < circleCount; i++) {
		printf("Circle %i: <radius> (in m)\n", i+1);
		double tempRadius = 0;
		int scanfFailureDetection = scanf("%lf", &tempRadius);
		if(tempRadius > 0 && scanfFailureDetection == 1) {
			circleArray[i].radius_m = tempRadius;
			circleArray[i].index = i + 1;
		}
		else {
			return EXIT_FAILURE;
		}
	}
}

void GetPerimeter(struct Circle *circleArray, int circleCount){
	int i;
	for(i = 0; i < circleCount; i++) {
		circleArray[i].perimeter_m = circleArray[i].radius_m * 2 * M_PI;

	}
}
void GetArea(struct Circle *circleArray, int circleCount){
	int i;
	for(i = 0; i < circleCount; i++) {
		circleArray[i].area_m2 = circleArray[i].radius_m * circleArray[i].radius_m * M_PI;
	}
}
double GetAverageArea(struct Circle *circleArray, int circleCount) {
	int i;
	double sumArea = 0;
	for(i = 0; i < circleCount; i++) {
		sumArea += circleArray[i].area_m2;
	}
	return sumArea/circleCount;
}
double GetMinArea(struct Circle *circleArray, int circleCount) {
	int i;
	double minArea = circleArray[0].area_m2;
	for(i = 1; i < circleCount; i++) {
		if(circleArray[i].area_m2 < minArea) {
			minArea = circleArray[i].area_m2;
		}
		}
	return minArea;
}
void SortCircleArray(struct Circle *circleArray,int circleCount) {
	int i;
	int j;
	for (i = 0 ; i < ( circleCount - 1 ); i++)
	  {
	    for (j = 0 ; j < circleCount - i - 1; j++)
	    {
	      if (circleArray[j].area_m2 > circleArray[j + 1].area_m2) /* For decreasing order use < */
	      {
	    	struct Circle tempCircle;//temp circle variable
	    	//save circleArray[j] in tempCircle
	    	tempCircle.area_m2 = circleArray[j].area_m2;
	    	tempCircle.index = circleArray[j].index;
	    	tempCircle.perimeter_m = circleArray[j].perimeter_m;
	    	tempCircle.radius_m = circleArray[j].radius_m;

	    	//load circleArray[j+1] in circleArray[j]
	    	circleArray[j].area_m2 = circleArray[j+1].area_m2;
	    	circleArray[j].index = circleArray[j+1].index;
	    	circleArray[j].perimeter_m = circleArray[j+1].perimeter_m;
	    	circleArray[j].radius_m = circleArray[j+1].radius_m;

	    	//load temped values in circleArray[j+1]
	    	circleArray[j+1].area_m2 = tempCircle.area_m2;
	    	circleArray[j+1].index = tempCircle.index;
	    	circleArray[j+1].perimeter_m = tempCircle.perimeter_m;
	    	circleArray[j+1].radius_m = tempCircle.radius_m;
	      }
	    }
	  }
}
void PrintMedian(struct Circle *circleArray, int circleCount) {
	if(circleCount %2 != 0) {
		printf("Circle area: %8.3f m²", circleArray[circleCount - ((circleCount - 1)/ 2) - 1].area_m2);
	}
	else {
		double median = (circleArray[(circleCount / 2) - 1].area_m2 + circleArray[circleCount / 2].area_m2) / 2;
		printf("Circle area: %8.3f m²", median);
	}
}
int GetRadiusesFromFile_m(struct Circle *circleArray,int *circleCount) {
	FILE *pFile = fopen( "./src/circles.txt", "r" );

	if(pFile != NULL) {
		double buffer = 0;
		int i = 0;
		while (EOF != fscanf(pFile, "%lf", &buffer)) {
			circleArray[i].radius_m = buffer;
			circleArray[i].index = i + 1;
			i++;
		}
		fclose(pFile);
		*circleCount = i;
	  }
	  else {
		  return EXIT_FAILURE;
	  }
	return EXIT_SUCCESS;
}
