/*
 ============================================================================
 Name        : circle_analyzer.c
 Author      : Tobias Gahleitner
 Version     : <Version>
 Copyright   : <Your copyright notice>
 Description : Circle statistics analyzer in C.
 ============================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "helpers.h"

int main(void) {

	//printf(" == Circle Analyzer ==\n\n");

	struct Circle circleArray[MAX_CIRCLE_COUNT] = {0}; //Create variable of Circle struct and init whole array with NULL;
	int circleCount = 0;

	if(GetRadiusesFromFile_m(circleArray, &circleCount) == EXIT_FAILURE) {
		printf("Error. File could not be opened.");
		return EXIT_FAILURE;
	}


	//circleCount++; //following loops go from 0 to < circleCount. circleCount was used as index before, so +1 makes it a 'real' count again.

	//Get perimeter of circles.
	GetPerimeter(circleArray, circleCount); //circleArray called by Ref

	//Get area of circles.
	GetArea(circleArray, circleCount); //circleArray called by Ref

	double avgArea_m2 =  GetAverageArea(circleArray, circleCount); //Get avg area
	double minArea_m2 = GetMinArea(circleArray, circleCount); //Get min area

	//print statistics
	printf("\n-------\n");
	printf("Average area: %8.3f\n", avgArea_m2);
	printf("Minimum area: %8.3f\n\n", minArea_m2);

	//print circle elements & properties
	int i;
	for(i = 0; i < circleCount; i++) {
		printf("Circle <%i>: radius: %8.3f m  -->  perimeter: %8.3f m, area: %8.3f m²\n",circleArray[i].index, circleArray[i].radius_m, circleArray[i].perimeter_m, circleArray[i].area_m2);

	}

	SortCircleArray(circleArray, circleCount); //Sort circles by area size. circleArray is called by Ref

	//print sorted array
	printf("\n\n");
	for(i = 0; i < circleCount; i++) {
			printf("Circle <%i>: area: %8.3f m²\n",circleArray[i].index, circleArray[i].area_m2);

		}

	printf("\n\n-------\nMedian: ");
	PrintMedian(circleArray, circleCount);//print median of list.
	printf("\n\n");


	return EXIT_SUCCESS;
}
