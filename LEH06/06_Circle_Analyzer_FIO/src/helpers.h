/*
 * helpers.h
 *
 *  Created on: Oct 18, 2017
 *      Author: es
 */

#ifndef HELPERS_H_
#define HELPERS_H_

#define MAX_CIRCLE_COUNT 100
#define FILE_PATH "../circles.txt";

//struct for circle properties
typedef struct Circle {
	int index;
	double radius_m;
	double perimeter_m;
	double area_m2;
};

int GetRadiusesFromFile_m(struct Circle *circleArray,int *circleCount); //reads radiuses from filestream

int GetCircleRadius_m(struct Circle *circleArray, int circleCount); //reads radiuses of all circleArray-elements. circleArray is called by reference.

void GetPerimeter(struct Circle *circleArray, int circleCount); //calculates perimeter of circle. circleArray is called by Ref

void GetArea(struct Circle *circleArray, int circleCount); //calculates area of cirlce. circleArray called by Ref

double GetAverageArea(struct Circle *circleArray, int circleCount); //returns avg area of all circles.

double GetMinArea(struct Circle *circleArray, int circleCount); //returns min arao of all circles.

void SortCircleArray(struct Circle *circleArray,int circleCount); //sorts insered circleArray using bubblesort-algorythm. result returned by call by ref.

void PrintMedian(struct Circle *circleArray, int circleCount);//outputs median element of list.

#endif /* HELPERS_H_ */
