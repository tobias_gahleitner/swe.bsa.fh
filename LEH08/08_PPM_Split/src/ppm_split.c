/*
 ============================================================================
 Name        : ppm_split.c
 Author      : <Your Name>
 Version     : <Version>
 Copyright   : <Your copyright notice>
 Description : Portable Pixel Map application supporting splitting files.
 ============================================================================
 */
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include"image.h"

int main(int argc, char *argv[]) {
	//
	// Load command line arguments.
	//
	char *inputFileName = "";
	char *outputFileName = "";

	int splitCount = 1;
	int opt;
    char *splitDirection = "";
	while ((opt = getopt(argc, argv, "o:i:s:d:")) != -1) {

			switch(opt) {
				// Input file name
				case 'i':
					inputFileName = optarg;
					break;

				// Output file name
				case 'o':
					outputFileName = optarg;
					break;
				case 's':
					splitCount = atoi(optarg);
					break;
                case 'd':
                    splitDirection = optarg;
				default:
					break;
			}
		}

    if(splitCount > MAX_SPLIT_COUNT) {
        printf("Error. Split-count to high. Max split-count: %d", MAX_SPLIT_COUNT);
        return EXIT_FAILURE;
    }

	struct tPPM inputImageStruct;
	inputImageStruct.fileName = inputFileName;

	struct tPPM *ptr_inputImageStruct = &inputImageStruct;

	if(GetInputFile(ptr_inputImageStruct) == EXIT_FAILURE) {
        return EXIT_FAILURE;
    }

    struct tPPM outputImages[MAX_SPLIT_COUNT] = {0};

	inputImageStruct.fileName = "test.ppm";
	//SaveImage(&inputImageStruct);
    if(strcmp(splitDirection, "h") == 0) {
        SplitImageHorizontal(&inputImageStruct, outputImages, splitCount, outputFileName);
    }
    else if(strcmp(splitDirection, "v") == 0) {
        SplitImageVertical(&inputImageStruct, outputImages, splitCount, outputFileName);
    }
    else {
        printf("Argument error. use ./<executable> -i <input file name> -o <output file name> [-s <n> -d <axis>]");
        return EXIT_FAILURE;
    }

	return EXIT_SUCCESS;
}
