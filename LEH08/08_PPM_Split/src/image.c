#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include"image.h"

int GetInputFile(struct tPPM *inputImageStruct) {
    FILE *inputImageFile = fopen(inputImageStruct->fileName, "r");


    if (inputImageFile != NULL) {
        fscanf(inputImageFile, "%s", inputImageStruct->version);
        fscanf(inputImageFile, "%d", &inputImageStruct->width);
        fscanf(inputImageFile, "%d", &inputImageStruct->height);
        fscanf(inputImageFile, "%d", &inputImageStruct->max_val);

        int i;
        int j;
        for (i = 0; i < inputImageStruct->height; i++) {
            for (j = 0; j < inputImageStruct->width; j++) {
                fscanf(inputImageFile, "%d %d %d ", &inputImageStruct->pixel[i][j].r, &inputImageStruct->pixel[i][j].g, &inputImageStruct->pixel[i][j].b);
            }
        }

        fclose(inputImageFile);

    } else {
        printf("Error occured while opening file.");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;

}
void SplitImageVertical(struct tPPM *inputImageStruct, struct tPPM *outputImages, int splitCount, char *outputName){

    int pixelStart = 0;
    int pixelStop = (inputImageStruct->width)/splitCount;
    int pixelAdd = pixelStop;

    strtok(outputName, "."); //Deletes everything on the right and including . of string

    for (int i = 0; i < splitCount; ++i) {
        if(i == splitCount -1) {
            pixelStop = inputImageStruct->width - 1;
            }

        outputImages[i].version[0] = inputImageStruct->version[0];
        outputImages[i].version[1] = inputImageStruct->version[1];
        outputImages[i].version[2] = inputImageStruct->version[2];
        outputImages[i].height = inputImageStruct->height;
        outputImages[i].width = pixelStop - pixelStart;
        outputImages[i].max_val = inputImageStruct->max_val;





        strcat(outputName, "-"); //Appends - to name
        char fileNumber[4];
        sprintf(fileNumber, "%d", i + 1); //Appends number to name
        strcat(outputName, &fileNumber);
        strcat(outputName, ".ppm"); //Appends .ppm to name
        outputImages[i].fileName = outputName; //Sets output name

        //

        for (int j = 0; j < inputImageStruct->height; ++j) {
            for (int k = pixelStart; k < pixelStop ; ++k) {
                outputImages[i].pixel[j][k - pixelAdd * i].r = inputImageStruct->pixel[j][k].r;
                outputImages[i].pixel[j][k - pixelAdd * i].g = inputImageStruct->pixel[j][k].g;
                outputImages[i].pixel[j][k - pixelAdd * i].b = inputImageStruct->pixel[j][k].b;
            }
        }

        pixelStart+= pixelAdd;
        pixelStop += pixelAdd;

        if(SaveImage(&outputImages[i]) == EXIT_FAILURE) {
            printf("Error ocurred at saving image(s)");
        }


        strtok(outputName, "-"); //Resets Output name
    }
}

void SplitImageHorizontal(struct tPPM *inputImageStruct, struct tPPM *outputImages, int splitCount, char *outputName) {

    int pixelStart = 0;
    int pixelStop = (inputImageStruct->height)/splitCount;
    int pixelAdd = pixelStop;

    strtok(outputName, "."); //Deletes everything on the right and including . of string

    for (int i = 0; i < splitCount; ++i) {
        if(i == splitCount -1) {
            pixelStop = inputImageStruct->width - 1;
        }

        outputImages[i].version[0] = inputImageStruct->version[0];
        outputImages[i].version[1] = inputImageStruct->version[1];
        outputImages[i].version[2] = inputImageStruct->version[2];
        outputImages[i].height = pixelStop - pixelStart;
        outputImages[i].width = inputImageStruct->width;
        outputImages[i].max_val = inputImageStruct->max_val;

        strcat(outputName, "-"); //Appends - to name
        char fileNumber[4];
        sprintf(fileNumber, "%d", i + 1); //Appends number to name
        strcat(outputName, &fileNumber);
        strcat(outputName, ".ppm"); //Appends .ppm to name
        outputImages[i].fileName = outputName; //Sets output name

        for (int j = pixelStart; j < pixelStop; ++j) {
            for (int k = 0; k < inputImageStruct->width ; ++k) {
                outputImages[i].pixel[j - pixelAdd * i][k].r = inputImageStruct->pixel[j][k].r;
                outputImages[i].pixel[j - pixelAdd * i][k].g = inputImageStruct->pixel[j][k].g;
                outputImages[i].pixel[j - pixelAdd * i][k].b = inputImageStruct->pixel[j][k].b;
            }
        }

        pixelStart+= pixelAdd;
        pixelStop += pixelAdd;

        if(SaveImage(&outputImages[i]) == EXIT_FAILURE) {
            printf("Error ocurred at saving image(s)");
        }


        strtok(outputName, "-"); //Resets Output name
    }
}



int SaveImage(struct tPPM *outputImage) {

	FILE *file = fopen(outputImage->fileName, "w");

	if( file != NULL) {
		fputs(strcat(outputImage->version, "\n"), file);
		fprintf(file, "%d ", outputImage->width);
		fprintf(file, "%d\n", outputImage->height);
		fprintf(file, "%d\n", outputImage->max_val);
		int i;
		int j;
		for(i = 0; i < outputImage->height; i++) {
			for(j = 0; j < outputImage->width; j++) {
				fprintf(file, "%d ", outputImage->pixel[i][j].r);
				fprintf(file, "%d ", outputImage->pixel[i][j].g);
				fprintf(file, "%d ", outputImage->pixel[i][j].b);
			}
			fprintf(file, "%s", "\n");
		}
        fclose(file);

	}
    else {
        return EXIT_FAILURE;
    }

	return EXIT_SUCCESS;
}
