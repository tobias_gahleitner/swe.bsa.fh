/*
 * image.h
 *
 *  Created on: Nov 16, 2017
 *      Author: es
 */

#ifndef IMAGE_H_
#define IMAGE_H_

#define MAX_WIDTH 200
#define MAX_HEIGHT 200

#define MAX_SPLIT_COUNT 20

typedef struct tPixel{
	int 			r; /**< Red pixel value. */
	int 			g; /**< Green pixel value. */
	int 			b; /**< Blue pixel value. */
};

typedef struct tPPM{
	char* 			fileName;		/**< Pointer to ppm file name. */
	char 			version[3]; /**< PPM Version. */
	int 			height;     /**< Image height. */
	int 			width;		/**< Image width. */
	int				max_val;	/**< Image max. color value. */
	struct tPixel 	pixel[MAX_HEIGHT][MAX_WIDTH];
	//struct tPPM *next;							/**< Image pixel (r,g,b) data. */
};

int GetInputFile(struct tPPM *inputImageStruct); //Reads Input File and stores it in inputImageStruct (Call by Ref)
void SplitImageHorizontal(struct tPPM *inputImageStruct, struct tPPM *outputImages, int splitCount, char *outputName); //Splits into seperate images and also calls SaveImage.
void SplitImageVertical(struct tPPM *inputImageStruct, struct tPPM *outputImages, int splitCount, char *outputName); //Splits into seperate images and also calls SaveImage.
int SaveImage(struct tPPM *outputImage) ; //Saves image to disk


#endif /* IMAGE_H_ */
