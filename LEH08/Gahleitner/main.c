/*
 ============================================================================
 Name        : ppm_split.c
 Author      : <Your Name>
 Version     : <Version>
 Copyright   : <Your copyright notice>
 Description : Portable Pixel Map application supporting splitting files.
 ============================================================================
 */
#include <stdlib.h>
#include <unistd.h>
#include"image.h"

int main(int argc, char *argv[]){

    //

    if(argc == 0)
    {
        return EXIT_FAILURE;
    }

    // Load command line arguments.
    //
    char *inputFileName;
    char *outputFileName;


    int splitCount = 0;
    int opt;
    while ((opt = getopt(argc, argv, "o:i:s:")) != -1) {

        switch(opt) {
            // Input file name
            case 'i':
                inputFileName = optarg;
                break;

                // Output file name
            case 'o':
                outputFileName = optarg;
                break;
            case 's':
                splitCount = atoi(optarg);
                break;

            default:
                break;
        }
    }

    struct tPPM inputImageStruct;
    inputImageStruct.fileName = inputFileName;

    struct tPPM *ptr_inputImageStruct = &inputImageStruct;

    GetInputFile(ptr_inputImageStruct);

    struct tPPM outputImages[MAX_SPLIT_COUNT] = {0};

    SplitImage(&inputImageStruct, outputImages, splitCount);


    return EXIT_SUCCESS;
}
