//
// Created by tobias on 26.11.17.
//

#ifndef GAHLEITNER_IMAGE_H
#define GAHLEITNER_IMAGE_H

#define MAX_WIDTH 200
#define MAX_HEIGHT 200

#define MAX_SPLIT_COUNT 20

typedef struct tPixel{
    int 			r; /**< Red pixel value. */
    int 			g; /**< Green pixel value. */
    int 			b; /**< Blue pixel value. */
};

typedef struct tPPM{
    char* 			fileName;		/**< Pointer to ppm file name. */
    char 			version[3]; /**< PPM Version. */
    int 			height;     /**< Image height. */
    int 			width;		/**< Image width. */
    int				max_val;	/**< Image max. color value. */
    struct tPixel 	pixel[MAX_WIDTH][MAX_HEIGHT];
    //struct tPPM *next;							/**< Image pixel (r,g,b) data. */
};

void GetInputFile(struct tPPM *inputImageStruct);
void SplitImage(struct tPPM *inputImageStruct, struct tPPM *outputImages, int splitCount);

#endif //GAHLEITNER_IMAGE_H
