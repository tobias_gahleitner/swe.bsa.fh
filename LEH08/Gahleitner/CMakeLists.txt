cmake_minimum_required(VERSION 3.8)
project(Gahleitner)

set(CMAKE_C_STANDARD 99)

set(SOURCE_FILES main.c image.c image.h)
add_executable(Gahleitner ${SOURCE_FILES})