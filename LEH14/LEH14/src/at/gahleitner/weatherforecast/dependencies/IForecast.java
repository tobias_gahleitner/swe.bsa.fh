package at.gahleitner.weatherforecast.dependencies;

import java.io.IOException;
import java.net.MalformedURLException;

public interface IForecast {
    void today() throws MalformedURLException, IOException;
    void tomorrow() throws MalformedURLException, IOException;
    void week() throws MalformedURLException, IOException;
}

