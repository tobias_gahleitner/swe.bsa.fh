package at.gahleitner.weatherforecast.dependencies;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;


public class WeatherForecastAdapter implements IForecast {

    private static final String apiKey = "e9b3e40b1174e2215b4d761f665ef1b5";
    private static final String[] url = {"http://api.openweathermap.org/data/2.5/forecast?q=", "&units=metric&APPID=" + apiKey};
    private String loc;

    public WeatherForecastAdapter(String loc){
        this.loc = loc;
    }

    @Override
    public void today() throws MalformedURLException, IOException {
        printWeather(1);

    }

    @Override
    public void tomorrow() throws MalformedURLException, IOException {
        printWeather(2);
    }

    @Override
    public void week() throws MalformedURLException, IOException{
        printWeather(5);
    }

    //MalfomedURLException actually included in IOException, but can be handled seperately.
    private void printWeather(int days) throws MalformedURLException, IOException {


        //Creates URL-object from static URL-segments.
        URL webUrl = new URL(url[0] + loc + url[1]);

        //Scanner used to get JSON-String from WEB
        Scanner webScanner = new Scanner(webUrl.openStream());
        String jsonString = webScanner.nextLine();
        webScanner.close();

        //Creates Parent-JasonObject from given json-String. Constructor is able to retrieve city + weather data from
        //given object
        JSONObject jResponse = new JSONObject(jsonString);

        //Creates new City instance.
        City city = new City(jResponse);

        //Prints city-info + weather-info in given period.
        city.printInfo(days);
    }
}
