package at.gahleitner.weatherforecast.dependencies;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;



/**
 * City class
 *
 * <P>This class saves Weatherforecasts for a location.
 *
 *
 * @author 	Tobias Gahleitner
 * @version V1.4
 *
 */

public class City {
    private JSONObject jResponse; //Stores JSONObject from URL
    private JSONObject jCity; //Stores specific city-data from URL
    private JSONArray jWeatherArr; //Stores weatherdata form URL
    private int id;
    private String name;
    private double lat;
    private double lon;
    private String country;
    private int population;
    private List<Weather> weatherList;




    City(JSONObject jResponse){
        this.jResponse = jResponse;
        this.jCity = jResponse.getJSONObject("city");

        //Loads city variables from JSON-Object
        instanceCity();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public String getCountry() {
        return country;
    }

    public int getPopulation() {
        return population;
    }

    public List<Weather> getWeatherList() {
        return weatherList;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public void setWeatherList(List<Weather> weatherList) {
        this.weatherList = weatherList;
    }
    
    private void instanceCity(){

        //Reads all arguments from JSON-Objects and stores them in instance-variables
        setId(jCity.getInt("id"));
        setName(jCity.getString("name"));
        setLat(jCity.getJSONObject("coord").getDouble("lat"));
        setLon(jCity.getJSONObject("coord").getDouble("lon"));
        setCountry(jCity.getString("country"));
        setPopulation(jCity.getInt("population"));


        jWeatherArr = jResponse.getJSONArray("list");

        weatherList = new ArrayList<>();

        //Adds weather to list.
        for(int i = 0; i < jWeatherArr.length(); i++){
            Weather tWeather = new Weather(jWeatherArr.getJSONObject(i));

            weatherList.add(tWeather);
        }

    }
    public void printInfo(int days){
        System.out.println("┌──────────────────────────────────────────────────────────────┐");
        System.out.println("│                                                              │");
        System.out.println("│                         Location Info                        │");
        System.out.println("│                                                              │");
        System.out.println("└──────────────────────────────────────────────────────────────┘\n");

        System.out.println("│   " + getName() + ", " + getCountry());
        System.out.println("│   Lat: " + getLat() + ", Long: " +getLon() + "\n");

        System.out.println("┌──────────────────────────────────────────────────────────────┐");
        System.out.println("│                                                              ");
        System.out.println("│                         Weather                              ");
        System.out.println("│                                                              ");
        System.out.println("│                                                              ");


        Date date = new Date();
        Date dateForeCastUntil = new Date(date.getTime() + (1000 * 60 * 60 * 24 * days));
        for(Weather weather : weatherList){

            if(weather.getDate().before(dateForeCastUntil)) {
                weather.printInfo();
            }

        }
    }
}
