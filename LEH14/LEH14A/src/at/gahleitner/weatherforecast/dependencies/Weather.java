package at.gahleitner.weatherforecast.dependencies;

import javax.json.*;

public class Weather {
    private JsonNumber id;
    private JsonString main;
    private JsonString description;
    private JsonString icon;

    Weather(JsonObject data){
        //TODO Implement
    }
    public long getId(){
        return id.longValue();
    }
    public String getMain(){
        return main.getString();
    }
    public String getDescription(){
        return description.getString();
    }
    public String getIcon(){
        return icon.getString();
    }
}
