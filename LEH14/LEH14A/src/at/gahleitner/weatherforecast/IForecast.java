package at.gahleitner.weatherforecast;

public interface IForecast {
    public void today();
    public void tomorrow();
    public void week();
}
