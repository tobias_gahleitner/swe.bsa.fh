package at.gahleitner.weatherforecast;

import java.util.Scanner;

/**
 * Weather forecasting using the interface {@link at.gahleitner.weatherforecast.IForecast}.
 * 
 * <P>This program allows a user to get weather forecast for one day, two days and a while,
 * of a given location.
 * 
 * <P> Usage:
 * 
 *   <P><b>java</b> WeatherForecast
 * 
 * @author 	<Your Name>
 * @version <Verison>
 *
 */
public class WeatherForecast {

	/**
	 * Application entry point.
	 * 
	 * @param args	Terminal arguments.
	 */
	public static void main(String[] args) {
		
		// TODO (1)
		// Setup application and user input.
		//
		Scanner sIn = new Scanner(System.in);

		
		// TODO (2)
		// Loop menu, unless user request differently.
		//
		do {


			// TODO (3)
			// Ask if user wants the weather forecast for the
			//
			//  * next day,
			//  * next two days,
			//  * next week, or
			//  * to quit the application.
			// 
			//                  ################################################################
			System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			System.out.println("@@                                                            @@");
			System.out.println("@@                      Weather Forecast                      @@");
			System.out.println("@@                                                            @@");
			System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");

			System.out.println("┌──────────────────────────────────────────────────────────────┐");
			System.out.println("│                                                              │");
			System.out.println("│        |1| Get the forecast for the next day.                │");
			System.out.println("│        |2| Get the forecast for tomorrow.                    │");
			System.out.println("│        |3| Get the forecast for the week.                    │");
			System.out.println("│        |4| Quit.                                             │");
			System.out.println("│                                                              │");
			System.out.println("└──────────────────────────────────────────────────────────────┘\n");

			String input = sIn.nextLine();
			if(input.equals("4")){
				break;
			}
			// TODO (4)
			// Quit, if requested.
			//

			// TODO (5)
			// Ask for the location of interest.
			//			
            System.out.println("For what location: ");
			String location = sIn.nextLine();
			// TODO (6)
			// Try to retrieve the weather forecast and parse/load data.
			//

			// TODO (7)
			// Differentiate between:
			//

			// TODO (7.1)
			// Forecast: 1 day. (default)
			//

			// TODO (7.2)
			// Forecast: 2 days.
			//

			// TODO (7.3)
			// Forecast: 1 week.
			//

			// TODO (7.1)
			// Handle MalformedURLException.
			//

			// TODO (7.2)
			// Handle IOException.
			//	

			// TODO (7.3)
			// Handle any other Exception that might occur.
			//

		}
		while(true);

		System.out.println("Thank you. Good Bye.");
		// TODO (9)
		// Shutdown.
		//
		
	}

}
