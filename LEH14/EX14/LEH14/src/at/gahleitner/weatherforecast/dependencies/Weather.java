package at.gahleitner.weatherforecast.dependencies;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;



public class Weather {
    private int dt;
    private String dt_txt;
    private double temp;
    private double temp_min;
    private double temp_max;
    private double pressure;
    private double humidity;
    private double wind_speed;
    private String weather_main;
    private String weather_description;
    private JSONObject jWeather;
    private Date date;
    
    public Weather(JSONObject jWeather){
        this.jWeather = jWeather;

        //Loads weather variables from JSON-object.
        instanceWeather();
    }


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getDt() {
        return dt;
    }

    public String getDt_txt() {
        return dt_txt;
    }

    public double getTemp() {
        return temp;
    }

    public double getTemp_min() {
        return temp_min;
    }

    public double getTemp_max() {
        return temp_max;
    }

    public double getPressure() {
        return pressure;
    }

    public double getHumidity() {
        return humidity;
    }

    public double getWind_speed() {
        return wind_speed;
    }

    public String getWeather_main() {
        return weather_main;
    }

    public String getWeather_description() {
        return weather_description;
    }

    public void setDt(int dt) {
        this.dt = dt;
    }

    public void setDt_txt(String dt_txt) {
        this.dt_txt = dt_txt;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public void setTemp_min(double temp_min) {
        this.temp_min = temp_min;
    }

    public void setTemp_max(double temp_max) {
        this.temp_max = temp_max;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public void setWind_speed(double wind_speed) {
        this.wind_speed = wind_speed;
    }

    public void setWeather_main(String weather_main) {
        this.weather_main = weather_main;
    }

    public void setWeather_description(String weather_description) {
        this.weather_description = weather_description;
    }
    
    private void instanceWeather(){

        //Stores variables from JSON-Object in instance-variables
        setDt(jWeather.getInt("dt"));
        setDt_txt(jWeather.getString("dt_txt"));

        JSONObject tJWMain = jWeather.getJSONObject("main");
        setTemp(tJWMain.getDouble("temp"));
        setTemp_min(tJWMain.getDouble("temp_min"));
        setTemp_max(tJWMain.getDouble("temp_max"));
        setPressure(tJWMain.getDouble("pressure"));
        setHumidity(tJWMain.getDouble("humidity"));



        JSONObject tJWWind = jWeather.getJSONObject("wind");
        setWind_speed(tJWWind.getDouble("speed"));

        JSONArray tJAWeather = jWeather.getJSONArray("weather");
        JSONObject tJWWeather = tJAWeather.getJSONObject(0);
        setWeather_main(tJWWeather.getString("main"));
        setWeather_description(tJWWeather.getString("description"));

        //Generating date from given String:
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {
            date = format.parse(dt_txt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
    public void printInfo(){

        //Prints weather-specific info.

        System.out.println("│   " + getDt_txt());
        System.out.println("│   Weather: " + getWeather_main() + ", " + getWeather_description());
        System.out.println("│   Temperature: avg: " + getTemp() + "°C");
        System.out.println("│   Pressure: " + getPressure() + "hPa");
        System.out.println("│   Humidity: " + getHumidity() + "%");
        System.out.println("│   Wind: " + getWind_speed() + "m/s");
        System.out.println("│");
        System.out.println("├──────────────────────────────────────────────────────────────┤");
        System.out.println("│                                                              ");
    }
        
}
