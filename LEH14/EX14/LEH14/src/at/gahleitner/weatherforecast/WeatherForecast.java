package at.gahleitner.weatherforecast;

import at.gahleitner.weatherforecast.dependencies.IForecast;
import at.gahleitner.weatherforecast.dependencies.WeatherForecastAdapter;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Scanner;

/**
 * WeatherData forecasting using the interface {@link at.gahleitner.weatherforecast.dependencies.IForecast}.
 *
 * <P>This program allows a user to get weather forecast for one day, two days and a while,
 * of a given location.
 *
 * <P> Usage:
 *
 *   <P><b>java</b> WeatherForecast
 *
 * @author 	Tobias Gahleitner
 * @version V1.4
 *
 */
public class WeatherForecast {

    /**
     * Application entry point.
     *
     * @param args	Terminal arguments.
     */
    public static void main(String[] args) {

        Scanner sIn = new Scanner(System.in);

        do {

            //sIn = new Scanner(System.in);

            System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
            System.out.println("@@                                                            @@");
            System.out.println("@@                      WeatherData Forecast                  @@");
            System.out.println("@@                                                            @@");
            System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n");

            System.out.println("┌──────────────────────────────────────────────────────────────┐");
            System.out.println("│                                                              │");
            System.out.println("│        |1| Get the forecast for the next 24 hours.           │");
            System.out.println("│        |2| Get the forecast for the next 48 hours.           │");
            System.out.println("│        |3| Get the forecast for the next 5 days.             │");
            System.out.println("│        |4| Quit.                                             │");
            System.out.println("│                                                              │");
            System.out.println("└──────────────────────────────────────────────────────────────┘\n");

            String input = sIn.nextLine();
            if(input.equals("4")){
                sIn.close();
                break;
            }

            System.out.println("For what location: ");
            String location = sIn.nextLine();

            //Uses IForecast Interface to get Weatherdata
            IForecast forecast = new WeatherForecastAdapter(location);

            try {
                switch (input) {
                    case "2":
                        forecast.tomorrow();
                        break;
                    case "3":
                        forecast.week();
                        break;
                    default:
                        forecast.today();
                        break;
                }

                System.out.println("Press Key to continue.");
                System.in.read();

                Util.clearScreen();
            }
            catch (MalformedURLException ex){
                System.out.println("Malformed Url.");
                ex.printStackTrace();
                break;

            }
            catch (IOException ex){
                System.out.println("IOExepion ocurred.");
                ex.printStackTrace();
                break;
            }
            catch (Exception ex){
                ex.printStackTrace();
                break;
            }
        }
        while(true);

        System.out.println("Thank you. Good Bye.");
    }

}
