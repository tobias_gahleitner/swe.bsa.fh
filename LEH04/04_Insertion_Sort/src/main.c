/*
 ============================================================================
 Name        : main.c
 Author      : Tobias Gahleitner
 Version     : V1.0
 Copyright   : <Your copyright notice>
 Description : Input sorting in C, using insertion sort algorithm.
 ============================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include"sort.h"

#define MAX_ARGUMENTS 100

int main(int argc, char *argv[]) {

	//Check argument count
	if(argc == 2 || argc > MAX_ARGUMENTS) {
		printf("Invalid argument count. Use ./<executable> <count> <number> ... <number>. Maximum count of arguments: %i", MAX_ARGUMENTS);
		return EXIT_FAILURE;
	}
	//Read count argument
	char *ptrConvert = "";
	int argCount = strtol(argv[1], &ptrConvert, 10);

	//Conversion error-detection
	if(*ptrConvert != '\0') {
		printf("First argument should be count of arguments. Use ./<executable> <count> <number> ... <number>. Maximum count of arguments: %i", MAX_ARGUMENTS);
		return EXIT_FAILURE;
	}

	//Check if enough arguments were given.
	if(argCount < (argc - 2)) {
		printf("Too few arguments given. Argument count should match arguments given.");
		return EXIT_FAILURE;
	}

	double zahlenUnsorted[argCount]; //Define array of double

	int i = 0;
	for(i; i < argCount; i++) {
		char *ptrConvertToDouble = "";
		zahlenUnsorted[i] = strtod(argv[i + 2], &ptrConvertToDouble); //COnverts string to double, error-detection via ptrConvert
		//Error detection
		if (*ptrConvertToDouble != '\0') { //Convert Error detection
				printf("Decimal could not be converted.");
				return EXIT_FAILURE;
		}
	}

	//Print input
	printf("Input:  [");

	for(i = 0; i < argCount; i++) //Print unsorted elements.
	{
		if(i != (argCount - 1)) { //Last element should not have , after it, so it checks if last argument reached.
			printf("%3.2f, ", zahlenUnsorted[i]);
		}
		else {
			printf("%3.2f]\n", zahlenUnsorted[i]); //Print last argument
		}
	}


	int sortResult = Sort(zahlenUnsorted, argCount); //Call sort function, which returns EXIT_SUCCESS if no exception occurred.

	//Error detection at sorting array
	if(sortResult != 0) {
		printf("Error occurred while sorting array.");
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
