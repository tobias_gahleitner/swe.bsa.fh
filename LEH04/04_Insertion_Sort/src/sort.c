/*
 * sort.c
 *
 *  Created on: Oct 10, 2017
 *      Author: Tobias Gahleitner
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sort.h"

int Sort(double zahlen[], int argCount) { //Able to sort array using insertion-sort algorythm


	double temp; //variable used to cache used element
	int i; //runtime-variable for outer for-loop
	int j; //runtime-variable for inner loop

	for (i = 0; i < argCount; i++){
			j = i;

			while (j > 0 && zahlen[j] < zahlen[j-1]){ //If number on the left side is bigger than number on the right side, change positions.
				  temp = zahlen[j]; //Cache used element
				  zahlen[j] = zahlen[j-1]; //Change order of elements.
				  zahlen[j-1] = temp; //Read old element from cache.
				  j--;
				  }
			}

	printf("Sorted: [");

	for(i = 0; i < argCount; i++) //Print sorted array
		{
			if(i != (argCount - 1)) {
				printf("%3.2f, ", zahlen[i]);
			}
			else {
				printf("%3.2f]", zahlen[i]);
			}
		}

	double sum = Sum(zahlen, argCount); //Call sum function
	double avg = sum/(double)argCount;

	printf("\n\nSum: %5.2f", sum);
	printf("\nAvg: %5.2f", avg);

	return EXIT_SUCCESS;
}
double Sum(double zahlen[], int argCount) { //Sum function.
	int i = 0;
	double sum = 0;
	for(i; i < argCount; i++) {
		sum += zahlen[i];
	}
	return sum;
}
