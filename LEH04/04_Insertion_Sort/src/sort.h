/*
 * sort.h
 *
 *  Created on: Oct 10, 2017
 *      Author: Tobias Gahleitner
 */

#ifndef SORT_H_
#define SORT_H_

double Sum(double zahlen[], int argCount); //Prototype for Sum function
int Sort(double zahlen[], int argCount); //Prototype for Sort function



#endif /* SORT_H_ */
