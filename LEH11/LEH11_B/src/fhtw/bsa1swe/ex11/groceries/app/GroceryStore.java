package fhtw.bsa1swe.ex11.groceries.app;

import java.util.ArrayList;

import fhtw.bsa1swe.ex11.groceries.actions.*;
import fhtw.bsa1swe.ex11.groceries.groceries.*;

public class GroceryStore {

    public static void main(String[] args) {

        // Create ArrayList of `Item`s to buy.
        ArrayList<Item> shoppingList = new ArrayList<>();

        //Add different items to list, during shopping.
        shoppingList.add(new Newspaper());
        shoppingList.add(new Apple());
        shoppingList.add(new IceCream());
        shoppingList.add(new IceCream());




        // Calculate amount to pay.
        double amountToPay = 0;
        for (IBuyable item : shoppingList) {
            amountToPay += item.getPrice();
        }

        System.out.println("Amount to pay = " + amountToPay + " €.");


        // Eat everything we can't take away.
        for (Item item : shoppingList) {
            if (!(item instanceof ITakeAway)) {
                IEatable food = (IEatable) item;
                food.eat();
            }
        }

        // Pack in take away goods.
        System.out.println("I'm going on a trip and I'm taking with me..");
        for (Item item : shoppingList) {
            if (item instanceof ITakeAway) {
                ITakeAway it = (ITakeAway) item;
                it.packIn();
            }
        }

        // Recycle everything we can.
        System.out.println("Keep it clean. Eat your greens. Don't be mean.");
        for (Item item : shoppingList) {
            if (item instanceof IRecyclable) {
                IRecyclable recyclate = (IRecyclable) item;
                recyclate.recycle();
            }
        }

    }

}
