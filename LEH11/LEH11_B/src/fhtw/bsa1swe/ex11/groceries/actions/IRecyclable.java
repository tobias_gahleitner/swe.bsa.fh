package fhtw.bsa1swe.ex11.groceries.actions;

public interface IRecyclable {
    public void recycle();
}
