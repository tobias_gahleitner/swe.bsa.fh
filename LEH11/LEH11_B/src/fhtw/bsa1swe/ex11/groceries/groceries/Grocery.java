package fhtw.bsa1swe.ex11.groceries.groceries;

import fhtw.bsa1swe.ex11.groceries.actions.IEatable;

public abstract class Grocery extends Item implements IEatable{

    public Grocery(long id) {
        super(id);
    }
}
