package fhtw.bsa1swe.ex11.groceries.groceries;

import fhtw.bsa1swe.ex11.groceries.actions.IBuyable;

public abstract class Item implements IBuyable{
    public static final long APPLE = 0;
    public static final long ICECREAM = 1;
    public static final long NEWSPAPER = 2;
    private long id;

    public long getId() {
        return id;
    }
    private void setId(long id) {
        this.id = id;
    }
    public Item(long id) {
        setId(id);
    }
}
