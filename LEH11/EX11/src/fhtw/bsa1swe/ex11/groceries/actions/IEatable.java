package fhtw.bsa1swe.ex11.groceries.actions;

public interface IEatable {
    public void eat();
}
