package fhtw.bsa1swe.ex11.groceries.actions;

public interface IBuyable {
    public double getPrice();
}
