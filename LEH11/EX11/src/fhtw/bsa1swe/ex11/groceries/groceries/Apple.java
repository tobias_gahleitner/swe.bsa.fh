package fhtw.bsa1swe.ex11.groceries.groceries;

import fhtw.bsa1swe.ex11.groceries.actions.ITakeAway;

public class Apple extends Grocery implements ITakeAway{

    private final double price = 0.5;

    public Apple() {
        super(Item.APPLE);
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public void eat() {
        System.out.println("Eat an apple....");
    }

    @Override
    public void packIn() {
        System.out.println("Put an apple in a bag.");
    }
}
