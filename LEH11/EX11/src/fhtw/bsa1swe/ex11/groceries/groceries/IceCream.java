package fhtw.bsa1swe.ex11.groceries.groceries;

public class IceCream extends Grocery {

    private final double price = 1.50;

    public IceCream() {
        super(Item.ICECREAM);
    }


    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public void eat() {
        System.out.println("Ice cream is always an option...");
    }
}
