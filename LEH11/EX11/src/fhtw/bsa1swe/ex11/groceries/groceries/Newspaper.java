package fhtw.bsa1swe.ex11.groceries.groceries;

import fhtw.bsa1swe.ex11.groceries.actions.IRecyclable;
import fhtw.bsa1swe.ex11.groceries.actions.ITakeAway;

public class Newspaper extends Item implements IRecyclable, ITakeAway{
    private final double price = 2.50;

    @Override
    public double getPrice() {
        return price;
    }
    public Newspaper(){
        super(Item.NEWSPAPER);
    }


    @Override
    public void recycle() {
        System.out.println("Recycle the newspaper.");
    }

    @Override
    public void packIn() {
        System.out.println("Put an newspaper in the bag.");
    }
}
