package life;

import java.util.ArrayList;
import creatures.*;

public class CircleOfLife {

    public static void main(String[] args) {

        // Create ArrayList of `Creature`s.
        ArrayList<Creature> creatures = new ArrayList<>();

        // TODO: Add creatures to the list.


        // All creatures are born ...
        for (Creature c : creatures) {
            c.birth();
        }

        // ... have lived some time ...
        long lifespan = (long) (Math.random() * 1000000000);
        for(long i = 0; i < lifespan; i++);

        // ... and die at the end.
        for (Creature c : creatures) {
            c.death();
        }

    }

}
