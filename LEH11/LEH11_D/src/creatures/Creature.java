package creatures;

import java.time.LocalDateTime;

public abstract class Creature {
    private LocalDateTime birthday;
    private LocalDateTime deathdeay;

    public abstract void birth();
    public void death() {
        System.out.println("Death...");
    }

    public String getSpecies() {
        return this.getClass().getSimpleName();
    }

}
