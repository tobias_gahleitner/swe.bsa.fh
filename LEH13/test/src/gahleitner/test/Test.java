package gahleitner.test;

import java.util.*;

public class Test {

    public static void main(String args[]){
        Set<String> swag = new HashSet<>();
        try {
            swag.add("Hello");
            swag.add("test");
            swag.add("Hello");
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }

        for(String text : swag){
            System.out.println(text);
        }
    }
}
