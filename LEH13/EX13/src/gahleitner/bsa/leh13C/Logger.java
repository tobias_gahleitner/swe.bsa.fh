package gahleitner.bsa.leh13C;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Write all real numbers into a new file and log all failed conversions.
 *
 * <P>This program writes all successfully converted numbers into a new
 * file and writes a log message for every failed conversion, including
 * the current line number.
 *
 * <P>Usage:
 *
 *   <P><b>java</b> Logger `file-name`
 *
 * @param	args	Terminal Arguments.
 *
 * @author 	Tobias gahleitner
 * @version	1.0
 *
 */
public class Logger {

    public static void main(String[] args) {

        try {
            String argument = args[0];
            int pointIndex = argument.lastIndexOf('.');
            String inputFileName = argument.substring(0, pointIndex);

            String outputFileName = inputFileName + "_clean.txt";
            String logFileName = inputFileName + "_log.txt";

            try (Scanner s = new Scanner(new File(argument));
                 PrintWriter prOut = new PrintWriter(new File(outputFileName));
                 PrintWriter prLog = new PrintWriter(new File(logFileName))){

                int i = 0; //Temp line index

                while(s.hasNextLine()) {

                    String line = s.nextLine(); //Temp line variable

                    try {
                        i++;

                        Double temp = Double.parseDouble(line);
                        prOut.println(temp); //If no Exception ocurred, write Double to file.

                    } catch (NumberFormatException e) { //Exception while parsing String to Double
                        String log = "L" + i + ": " + line;
                        prLog.println(log); //Add log to log file
                        System.out.println(log);
                    }

                }
            } catch (FileNotFoundException e) { //Exception of Scanner or PrintWriters, when file not found.
                System.out.println("Error ocurred while opening file(s).");
                e.printStackTrace();
            }



        } catch (IndexOutOfBoundsException ex){ //Exception when no arguments were given.
            System.out.println("No filename given. Please use java gahleitner.bsa.leh13C.Logger <input file name>");
            System.exit(1);
        }
    }
}

