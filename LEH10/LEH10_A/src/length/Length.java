/*
 =============================================================================================
 Name        : Length.java
 Author      : Tobias Gahleitner
 Version     : <Version>
 Copyright   : <Your copyright notice>
 Description : `Length` storing measurement results, and providing conversion operations.
 =============================================================================================
 */
package length;

public class Length {

    // --- Eigenschaften ---

    public static final String INCH = "in";
    public static final String KM = "km";
    public static final String M = "m";

    private String mUnit;
    private double mValue;

    // --- Konstruktoren

    public Length() { //default constructor
        this(0, M); //Overload constructor (initializes it with 0m.)
    }
    public Length(double length, String unit) { //advanced constructor
        setmValue(length);
        setmUnit(unit);
    }



    // --- Methoden ---

    public double getmValue() {
        return mValue;
    }
    public String getmUnit() {
        return mUnit;
    }

    public void setmUnit(String mUnit) {
        this.mUnit = mUnit;
    }
    public void setmValue(double mValue) {
        this.mValue = mValue;
    }

    public double convertToMeter() {

        try {

            double meter;
            if (INCH.equals(getmUnit())) {
                meter = getmValue() * 0.0254;
            } else if (KM.equals(getmUnit())) {
                meter =  getmValue() * 1000;
            } else if (M.equals(getmUnit())) {
                meter = getmValue();
            } else {
                throw new Exception("Unit error.");
            }
            return meter;
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
            return 0;
        }
    }
    public double differenceTo(Length length) {

        return (this.convertToMeter() - length.convertToMeter()); //returns difference
    }
    public void printInformation() { //Prints information to terminal
        System.out.println(getmValue() + getmUnit());
    }
	
}
