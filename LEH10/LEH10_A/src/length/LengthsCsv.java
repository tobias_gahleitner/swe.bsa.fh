/*
 ============================================================================
 Name        : LengthsCsv.java
 Author      : Tobias Gahleitner
 Version     : <Version>
 Copyright   : <Your copyright notice>
 Description : Reads length records (from *.csv) and writes them to new file 
               with converted value (in m) and calculated difference to 
               reference length.
 ============================================================================
 */
package length;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class LengthsCsv {

	public static final String DELIMITER = ";";
	public static final int INPUT_POS = 0;
	public static final int OUTPUT_POS = 1;
	public static final int LENGTH_VAL_POS = 2;
	public static final int LENGTH_UNIT_POS = 3;
	
	public static void main(String[] args) {

		String pathInput;
		String partOutput;

		Length lCompare = new Length();

		ArrayList<Length> lengths = new ArrayList<Length>(); //List of type Length, where lengths from csv file are stored


		try {
			if(args.length != 4) {
				throw new Exception("To few/many arguments. Use java <package.class> <input file name> <output file name> <value> <unit>"); //If too much or too less arguments given, throw a new error.
			}

			pathInput = args[INPUT_POS]; //Reads path from given terminal-argument.
			partOutput = args[OUTPUT_POS];
			lCompare.setmValue(Double.parseDouble(args[LENGTH_VAL_POS])); //Reads value and stores it into Length object.
			lCompare.setmUnit(args[LENGTH_UNIT_POS]);

			if (pathInput != null) {

				File file = new File(pathInput);
				Scanner s = new Scanner(file); //Opens file to read from it.
				readFromFile(s, lengths); //Calls readFromFile function.
				s.close();

			}
			else {
				throw new Exception("Input file path error.");
			}
			if (partOutput != null) {

				File file = new File(partOutput);
				PrintWriter pr = new PrintWriter(file); //Opens file to write to it.
				writeToFile(pr, lengths, lCompare);
				pr.close();
			}
			else {
				throw new Exception("Output file path error.");
			}

		}
		catch (Exception ex) {
			System.out.println(ex.getMessage()); //
		}

	}
	
	/**
	 * @brief Reads from `stream` and adds entries to `list`.
	 * 
	 * @param stream Scanner providing read access to file.
	 * @param list   ArrayList to store Length entries.
	 */
	public static void readFromFile(Scanner stream, ArrayList<Length> list) {
		
		// Loop until last line.
		while (stream.hasNextLine()) {

			String[] line = stream.nextLine().split(DELIMITER); //Reads line at DELIMITER and stores it into String-array.

			list.add(new Length(Double.parseDouble(line[0].trim()), line[1].trim())); //Adds next length + unit into Length ArrayList. Trim removes spaces before and after Value/Unit.

		}
	}
	
	/**
	 * @brief Writes entries of `list` to `stream`.
	 * 
	 * @param stream PrintWriter providing write access to file.
	 * @param list	 ArrayList of Length entries.
	 */
	public static void writeToFile(PrintWriter stream, ArrayList<Length> list, Length other) {

		for(Length length : list) { //foreach loop
			stream.println(length.getmValue() + DELIMITER + length.getmUnit() + DELIMITER + length.convertToMeter() + DELIMITER + length.differenceTo(other)); //Writes new line to csv file, uses given delimiter (Const).
		}

	}

}
