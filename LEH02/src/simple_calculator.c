/*
 ============================================================================
 Name        : simple_calculator.c
 Author      : Tobias Gahleitner
 Version     : V1.0
 Copyright   : ©Tobias Gahleitner 2017
 Description : <Description>
 ============================================================================
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>

//Prototypes of used functions
double Add(double zahl1, double zahl2);
double Sub(double zahl1, double zahl2);
double Mul(double zahl1, double zahl2);
double Div(double zahl1, double zahl2);



int main(int argc, char *argv[]) {

	//Check argument count
	if(argc != 4) {
		printf("Error. Arguments missing.");
		return EXIT_FAILURE;
	}


	char *ptr_checkConvert1 = "";

	double zahl1 = strtol(argv[1], &ptr_checkConvert1, 10); //Read first number from console

	//Convert Error Detection
	if (*ptr_checkConvert1 != '\0') {
		printf("Error. Argument 1 not a number.");
		return EXIT_FAILURE;
	}

	char * ptr_checkConvert2 = "";

	double zahl2 = strtol(argv[3], &ptr_checkConvert2, 10); //Read second number from console

	//Convert error detection
	if (*ptr_checkConvert2 != '\0') {
			printf("Error. Argument 3 not a number.");
			return EXIT_FAILURE;
		}

	double erg;

	//Identify operator
	if (strcmp("+", argv[2]) == 0) {
		erg = Add(zahl1, zahl2);
	} else if (strcmp("-", argv[2]) == 0) {
		erg = Sub(zahl1, zahl2);
	} else if (strcmp("*", argv[2]) == 0) {
		erg = Mul(zahl1, zahl2);
	} else if (strcmp("/", argv[2]) == 0) {
		erg = Div(zahl1, zahl2);
	} else {
		printf("Error! Wrong operator.");
		return EXIT_FAILURE;
	}

	//Print result
	printf("%.2f ", zahl1);
	printf(argv[2]);
	printf(" %.2f = %.2f\n", zahl2, erg);

	return EXIT_SUCCESS;
}


double Add(double zahl1, double zahl2) {
	return zahl1 + zahl2;
}
double Sub(double zahl1, double zahl2) {
	return zahl1 - zahl2;
}
double Mul(double zahl1, double zahl2) {
	return zahl1 * zahl2;
}
double Div(double zahl1, double zahl2) {
	return zahl1 / zahl2;
}

