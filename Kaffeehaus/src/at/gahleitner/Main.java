package at.gahleitner;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        System.out.println("Willkommen im Kaffeehaus Java.");

        
        ArrayList<Speise> speisenListe = new ArrayList<>();

        speisenListe.add(new Speise("Hofstättner Granit", 3.50));
        speisenListe.add(new Speise("Zwickl", 4));

        for (Speise speise : speisenListe) {
            speise.PrintSpeise();
        }

        System.out.println();

        ArrayList<Person> personenListe = new ArrayList<>();
        personenListe.add(new Person("Ansphie", 18));

        for (Person person : personenListe) {
            person.PrintPerson();
        }
        //speisenListe.
        //ArrayList<String> elemente = new ArrayList<String>();

        //int test[] = new int[10];
        //test[0] = 13;
        //System.out.println(test[0]);

    }
}
