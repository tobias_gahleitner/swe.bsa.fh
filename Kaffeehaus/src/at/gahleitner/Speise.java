package at.gahleitner;

public class Speise {
    private String name;
    private double preis;

    public double getPreis() {
        return preis;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setPreis(double preis) {
        this.preis = preis;
    }

    public Speise(String name, double preis) {
        setName(name);
        setPreis(preis);
    }

    public void PrintSpeise() {
        System.out.println(getName() + " " + getPreis() + "€");
    }
}
