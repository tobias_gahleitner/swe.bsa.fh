package at.gahleitner;

public class Person {
    private String name;
    private int alter;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setAlter(int alter) {
        this.alter = alter;
    }

    public int getAlter() {
        return alter;
    }
    public Person(String name, int alter) {
        setName(name);
        setAlter(alter);
    }
    public void PrintPerson() {
        System.out.println(getName() + ", " + getAlter());
    }
}
