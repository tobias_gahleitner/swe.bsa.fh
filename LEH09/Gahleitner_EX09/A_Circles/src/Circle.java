package at.gahleitner.leh09_a;

public class Circle {
    // --- Felder ---
    private String mColor;
    private double mRadius;


    // --- Methoden ---
    public double getmRadius() {
        return mRadius;
    }
    public String getmColor() {
        return mColor;
    }
    public void setmColor(String mColor) {
        this.mColor = mColor;
    }
    public void setmRadius(double mRadius) {
        this.mRadius = mRadius;
    }


    public double calculateArea() {
        return Math.PI * getmRadius() * getmRadius();
    }
    public double calculateAreaDifference(Circle circle) {
        return Math.PI * this.getmRadius() * this.getmRadius() - (Math.PI * circle.getmRadius() * circle.getmRadius());
    }
    public boolean hasColorAs(Circle circle){ //Checks if color of 2 circles is the same
        if(circle.getmColor() == this.getmColor())
            return true;
        else
            return false;
    }
    public void printInformation() { //Prints information to console
        System.out.println("Circle radius: " + getmRadius() + ", area: " + Math.round(calculateArea()) +  ", color: " + getmColor());

    }


    // --- Konstruktoren ---
    public Circle(){ //default constructor

    }
    public Circle(double mRadius, String mColor) { //advanced constructor used for initializing at start
        setmRadius(mRadius);
        setmColor(mColor);
    }
}
