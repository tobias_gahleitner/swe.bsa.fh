package at.gahleitner.leh09_b;

public class Main {

    public static void main(String[] args) {
        Length l1 = new Length(13.14, Length.KM); //Creates new instance and initializes it
        Length l2 = new Length(); //Creates new instance, but does not initialize it

        l2.setmValue(110000); //Sets Value
        l2.setmUnit(Length.INCH); //Sets Unit from given Units stored in Length class (static readonly)

        Length l3 = new Length(l1.differenceTo(l2), Length.M);

        System.out.print("L1: ");
        l1.printInformation();

        System.out.print("L2: ");
        l2.printInformation();

        System.out.print("L3: ");
        l3.printInformation();
    }
}
