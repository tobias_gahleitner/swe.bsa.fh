package at.gahleitner.leh09_b;

public class Length {

    // --- Felder ---

    public static final String INCH = "in";
    public static final String KM = "km";
    public static final String M = "m";

    private String mUnit;
    private double mValue;

    // --- Methoden ---

    public double getmValue() {
        return mValue;
    }
    public String getmUnit() {
        return mUnit;
    }

    public void setmUnit(String mUnit) {
        this.mUnit = mUnit;
    }
    public void setmValue(double mValue) {
        this.mValue = mValue;
    }

    public double convertToMeter() { //

        if(getmUnit() == INCH) {
            return getmValue() * 0.0254;
        }
        else if(getmUnit() == KM) {
            return getmValue() * 1000;
        }
        else if(getmUnit() == M) {
            return  getmValue();
        } else {
            return -1;
        }
    }
    public double differenceTo(Length length) {

        return this.convertToMeter() - length.convertToMeter(); //returns difference
    }
    public void printInformation() { //Prints information to terminal
        System.out.println(getmValue() + getmUnit());
    }

    // --- Konstruktoren

    public Length() { //default constructor

    }
    public Length(double length, String unit) { //advanced constructor
        setmValue(length);
        setmUnit(unit);
    }

}
