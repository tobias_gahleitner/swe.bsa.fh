package at.gahleitner.leh09_b;

public class Length {

    // --- Eigenschaften ---

    public static final String INCH = "in";
    public static final String KM = "km";
    public static final String M = "m";

    private String mUnit;
    private double mValue;

    // --- Konstruktoren

    public Length() { //default constructor
        this(0, M); //Overload constructor
    }
    public Length(double length, String unit) { //advanced constructor
        setmValue(length);
        setmUnit(unit);
    }



    // --- Methoden ---

    public double getmValue() {
        return mValue;
    }
    public String getmUnit() {
        return mUnit;
    }

    public void setmUnit(String mUnit) {
        this.mUnit = mUnit;
    }
    public void setmValue(double mValue) {
        this.mValue = mValue;
    }

    public double convertToMeter() { //

        if(getmUnit().equals(INCH)) { //WICHTIG MIT EQUALS
            return getmValue() * 0.0254;
        }
        else if(getmUnit().equals(KM)) {
            return getmValue() * 1000;
        }
        else if(getmUnit().equals(M)) {
            return  getmValue();
        } else {
            return -1;
        }
    }
    public double differenceTo(Length length) {

        return this.convertToMeter() - length.convertToMeter(); //returns difference
    }
    public void printInformation() { //Prints information to terminal
        System.out.println(getmValue() + getmUnit());
    }



}
