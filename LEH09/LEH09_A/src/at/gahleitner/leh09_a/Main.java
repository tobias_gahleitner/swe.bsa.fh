package at.gahleitner.leh09_a;

public class Main {

    public static void main(String[] args) {
        Circle c1 = new Circle(); //Creates new instance, but does not initialize it
        Circle c2 = new Circle(13, "red"); //Creates new instance and initializes it

        c1.setmRadius(14); //Sets radius
        c2.setmColor("blue"); //Sets color

        Circle c3 = c1; //sets c3 = c1

        System.out.println("c3 same color as c1: " + c3.hasColorAs(c1)); //Prints color difference
        System.out.println("c3 same color as c2: " + c3.hasColorAs(c2));
        c2.printInformation();

        double areaDiv = c1.calculateAreaDifference(c2);

        System.out.println("Area difference between c1 and c2: " + Math.round(areaDiv)); //Prints area difference
    }
}
